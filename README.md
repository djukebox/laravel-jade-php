## LaravelJadePHPServiceProvider
Adds Laravel 5 JadePHP view engine with [kylekatarnls](https://github.com/kylekatarnls) [Jade.php](https://github.com/kylekatarnls/jade-php).

## Installation

Require this package with composer:

```
composer require djukebox/laravel-jade-php
```

Add the ServiceProvider to the providers array in config/app.php:

```
'Cve\LaravelJadePHPServiceProvider',
```

## Usage

Create views as always but in Jade syntax

## License

LaravelJadePHPServiceProvider is licensed under the [MIT license](http://opensource.org/licenses/MIT).